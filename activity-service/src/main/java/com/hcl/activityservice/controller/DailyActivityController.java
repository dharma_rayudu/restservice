package com.hcl.activityservice.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.activityservice.dto.DailyActivityDTO;
import com.hcl.activityservice.entity.DailyActivity;
import com.hcl.activityservice.request.ActivityRequest;
import com.hcl.activityservice.service.DailyActivityService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/dailyactivity-data/v1/activities")
public class DailyActivityController {
	
	@Autowired
	private DailyActivityService dailyActivityService;
	
	@PostMapping
	public ResponseEntity<DailyActivityDTO> addDailyActivities(@RequestBody List<DailyActivityDTO> dailyActivity){
		log.info("Inside addDailyActivities of DailyActivityController");
		
		return dailyActivityService.addDailyActivity(dailyActivity);
	}
	
	@GetMapping
	public List<DailyActivityDTO> getAllDailyActivities(){
		log.info("Inside getAllDailyActivities of DailyActivityController");
		
		return dailyActivityService.getAllDailyActivitis();
	}
	
	@PutMapping("/{id}")
	public DailyActivityDTO updateActivities(@PathVariable Integer id, @RequestBody DailyActivity activity) {
		return dailyActivityService.updateDailyActivities(id, activity);
	}
	
	@GetMapping("/date/{date}")
	public List<DailyActivityDTO> getActivityByDate(@PathVariable 
			@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate date){
		return dailyActivityService.getActivityByDate(date);
				
	}
	
	@GetMapping("/{code}")
	public List<DailyActivityDTO> getActivityByCode(@PathVariable("code") Integer code) { 
		System.out.println("Calling getActivityByCode method ");
		  return dailyActivityService.getDailyActivity(code);
    }
	
	@PostMapping("/activity")
	public List<DailyActivityDTO> getActivityByDates(@RequestBody ActivityRequest request){
		return dailyActivityService.getActivityDataUsingDates(request.getFromDate(), request.getToDate());
	}

}
