package com.hcl.activityservice.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.activityservice.entity.DailyActivity;

@Repository
public interface DailyActivityRepository extends JpaRepository<DailyActivity, Integer> {

	List<DailyActivity> findByCode(Integer code);

	List<DailyActivity> findByDate(LocalDate date);

	//DailyActivity findByemp_name(String name);
	
	@Query("select d from DailyActivity d where d.date between :fromDate and :toDate ")
	List<DailyActivity> findByDateBetween(@Param(value = "fromDate") LocalDate fromDate, @Param(value = "toDate") LocalDate toDate);

	//findByCodeAndDate(Integer code Loca)
}
