package com.hcl.activityservice.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.hcl.activityservice.dto.DailyActivityDTO;
import com.hcl.activityservice.entity.DailyActivity;

public interface DailyActivityService {
	public ResponseEntity<DailyActivityDTO> addDailyActivity(List<DailyActivityDTO> dailyActivity);
	
	public List<DailyActivityDTO> getAllDailyActivitis();
	
	public List<DailyActivityDTO> getDailyActivity(Integer code);
	public void deleteDailyActiviry(Long id);
	public List<DailyActivityDTO> getAllPendingStatus();
	//public DailyActivityDTO getActivityByName(String code);
	public DailyActivityDTO updateDailyActivities(Integer id, DailyActivity activities);
	public List<DailyActivityDTO> getActivityByDate(LocalDate date);
	
	public List<DailyActivityDTO> getActivityDataUsingDates(LocalDate fromDate, LocalDate toDate);
	
	public List<DailyActivityDTO> getActivityDataUsingNameAndDates(String name, LocalDate fromDate, LocalDate toDate);

}
