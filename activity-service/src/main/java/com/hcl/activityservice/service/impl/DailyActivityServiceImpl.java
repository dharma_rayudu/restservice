package com.hcl.activityservice.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hcl.activityservice.dto.DailyActivityDTO;
import com.hcl.activityservice.entity.DailyActivity;
import com.hcl.activityservice.repository.DailyActivityRepository;
import com.hcl.activityservice.service.DailyActivityService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DailyActivityServiceImpl implements DailyActivityService {
//	long millis=System.currentTimeMillis();  
//	java.sql.Date date=new java.sql.Date(millis); 
	 LocalDate date = LocalDate.now(); 


	@Autowired
	private DailyActivityRepository dailyActivityRepository;
	

	@Override
	public ResponseEntity<DailyActivityDTO> addDailyActivity(List<DailyActivityDTO> dailyActivity) {
		log.info("Inside addDailyActivity of DailyActivityServiceImpl");

		List<DailyActivity> listDailyActivity = dtoToEntityConverter(dailyActivity);
		dailyActivityRepository.saveAll(listDailyActivity);
		return new ResponseEntity<DailyActivityDTO>(HttpStatus.CREATED);
	}

	@Override
	public List<DailyActivityDTO> getAllDailyActivitis() {
		log.info("Inside getAllDailyActivitis of DailyActivityServiceImpl");

		List<DailyActivity> listDaily = dailyActivityRepository.findAll();
		return entityToDtoConverter(listDaily);
	}

	/*
	 * @Override public DailyActivityDTO getDailyActivity(Long employeeCode) { //
	 * TODO Auto-generated method stub return null; }
	 */

	@Override
	public void deleteDailyActiviry(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<DailyActivityDTO> getAllPendingStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	private List<DailyActivity> dtoToEntityConverter(List<DailyActivityDTO> dailyActivity) {
		List<DailyActivity> dailyList = new ArrayList<>();

		dailyActivity.forEach(activity -> {
			//activity.setDate(date);
			DailyActivity activityD = new DailyActivity();
			BeanUtils.copyProperties(activity, activityD);
			dailyList.add(activityD);
		});
		return dailyList;
	}

	private List<DailyActivityDTO> entityToDtoConverter(List<DailyActivity> dailyActivity) {
		List<DailyActivityDTO> dailyDtoList = new ArrayList<>();

		dailyActivity.forEach(activity -> {
			DailyActivityDTO dto = new DailyActivityDTO();
			BeanUtils.copyProperties(activity, dto);
			dailyDtoList.add(dto);

		});
		return dailyDtoList;
	}

//	@Override
//	public DailyActivityDTO getDailyActivityByCode(Integer code) {
//		
//		DailyActivity activity= dailyActivityRepository.findByCode(code);
//		DailyActivityDTO dto = new DailyActivityDTO();
//		BeanUtils.copyProperties(activity, dto);
//		return dto;
//	}

	@Override
	public DailyActivityDTO updateDailyActivities(Integer id, DailyActivity activities) {
		
		DailyActivity activity= dailyActivityRepository.findById(id).get();
		activity.setDate(date);
		activity.setDescription(activities.getDescription());
		activity.setStatus(activities.getStatus());
		dailyActivityRepository.save(activity);
		
		DailyActivityDTO dto = new DailyActivityDTO();
		BeanUtils.copyProperties(activity, dto);
		return dto;
	}

	@Override
	public List<DailyActivityDTO> getActivityByDate(LocalDate date) {
		List<DailyActivity> activity = dailyActivityRepository.findByDate(date);
		
		List<DailyActivityDTO> lDto = new ArrayList<>();
		activity.forEach(activityData  ->{
			DailyActivityDTO dto = new DailyActivityDTO();
			BeanUtils.copyProperties(activityData, dto);
			lDto.add(dto);
		});
		
		return lDto;
	}

	@Override
	public List<DailyActivityDTO> getDailyActivity(Integer code) {
		List<DailyActivity> activity = dailyActivityRepository.findByCode(code);
		return entityToDtoConverter(activity);
	}

	@Override
	public List<DailyActivityDTO> getActivityDataUsingDates(LocalDate fromDate, LocalDate toDate) {
		
		return entityToDtoConverter(dailyActivityRepository.findByDateBetween(fromDate, toDate));
	}

	@Override
	public List<DailyActivityDTO> getActivityDataUsingNameAndDates(String name, LocalDate fromDate, LocalDate toDate) {
		
		return null;
	}

	/*
	 * @Override public DailyActivityDTO getActivityByName(String name) {
	 * 
	 * DailyActivity activity = dailyActivityRepository.findByemp_name(name);
	 * 
	 * log.info("What is the activity Data:"); log.info("Data: "+ activity);
	 * DailyActivityDTO dto = new DailyActivityDTO();
	 * BeanUtils.copyProperties(activity, dto); return dto; }
	 */

}
