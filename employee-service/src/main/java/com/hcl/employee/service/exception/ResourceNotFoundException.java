package com.hcl.employee.service.exception;

public class ResourceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ResourceNotFoundException(Integer code) {
		super("No Resource Found:"+ code);
	}

}
