package com.hcl.employee.service.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.employee.service.dto.EmployeeDTO;
import com.hcl.employee.service.entity.Employee;
import com.hcl.employee.service.feignI.DailyActivityClient;
import com.hcl.employee.service.response.EmployeeActivites;
import com.hcl.employee.service.service.EmployeeService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("employee-data/v1/employee")
@Slf4j
@Validated
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private DailyActivityClient dailyTest;
	
	@Autowired
	Environment environment;

	@PostMapping
	public ResponseEntity<EmployeeDTO> saveEmployee(@RequestBody @Valid @NotEmpty(message = "List cannot be empty") List<@Valid EmployeeDTO> employee) {
		log.info("Inside saveEmployee method of EmployeeController");
		
		return employeeService.addEmployee(employee);

	}

	@GetMapping
	public List<EmployeeDTO> getAllEmployees(){
		log.info("Inside saveEmployee method of EmployeeController");
		
		return employeeService.getAllEmployees();
	}
	
	@GetMapping("/{code}")
	public EmployeeDTO getEmployee(@PathVariable Integer code) {
		log.info("Inside getEmployee method of EmployeeController");
		
		return employeeService.getEmployeeByCode(code);
	}
	
	@PutMapping("/{code}")
	public EmployeeDTO updateEmployee(@PathVariable Integer code, @RequestBody Employee employee) {
		log.info("Inside updateEmployee method of EmployeeController");
		
		return employeeService.updateEmployee(code, employee);
	}
	
	@DeleteMapping("/{code}")
	public void deleteEmployee(@PathVariable Integer code) {
		log.info("Inside deleteEmployee method of EmployeeController");
		employeeService.deleteEmployee(code);
	}
	
	@GetMapping("experiance/{experiance}")
	public List<EmployeeDTO> getEmployeeByExperiance(@PathVariable("experiance") Integer experiance){
		log.info("Inside getEmployeeByExperiance method of EmployeeController");
		return employeeService.getEmployeesByExperiance(experiance);
	}
	
	/*
	 * @GetMapping("experiance/{designation}/{experiance}") public List<EmployeeDTO>
	 * getEmployeeByDesignationAndExperiance(@PathVariable("designation") String
	 * designation, @PathVariable("experiance") String experiance){ return
	 * employeeService.getEmployeeByDesignationAndExperiance(designation,
	 * experiance); }
	 */
	
	/*
	 * @GetMapping("/activeEmployees/{isActive}") public List<EmployeeDTO>
	 * getActiveEmployees(@PathVariable("isActive") boolean isActive){ return
	 * employeeService.getActiveEmployees(isActive); }
	 */
	
	@GetMapping("/activites/{code}")
	public EmployeeActivites getEmployeeActivites(@PathVariable("code") Integer code) {
		log.info("From Which port@@@@@@@@@:"+ environment.getProperty("local.server.port"));
		return employeeService.getEmployeeActivites(code);
	}
	
	
}
