package com.hcl.employee.service.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO {

	@Digits(integer = 10, fraction = 1, message = "Employee Code should be less than 10 digits")
	private Integer code;
	
	@NotEmpty(message = "Employee should not be empty")
	@Size(min = 2, max = 100, message = "Employee name size must be between 2 and 100")
	private String name;

	@NotEmpty(message = "Employee designation should not be empty")
	@Size(min = 5, max = 100, message = "Employee designation size must be between 5 and 100")
	private String jobTitle;

	@NotEmpty(message = "Employee emailId should not be empty")
	@Email
	private String emailId;

	@Digits(integer = 2, fraction = 2, message = "Employee Experience should be maximum of 2 digits")
	private Integer experiance;

	//@NotEmpty(message = "Employee phone number should not be empty")
	//@Pattern(regexp = "^[0-9]{10}$", message = "Phone number should be of 10 digits")
	private Long phoneNumber;

	private String location;
	
	private String project_status;	
}
