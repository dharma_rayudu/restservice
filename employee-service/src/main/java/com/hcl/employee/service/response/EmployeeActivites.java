package com.hcl.employee.service.response;

import java.util.ArrayList;
import java.util.List;

import com.hcl.employee.service.dto.DailyActivityDTO;
import com.hcl.employee.service.entity.Employee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


public class EmployeeActivites {
	
	private Employee emp;
	private List<DailyActivityDTO> activities = new ArrayList<>();
	
	public Employee getEmp() {
		return emp;
	}
	public void setEmp(Employee emp) {
		this.emp = emp;
	}
	public List<DailyActivityDTO> getActivities() {
		return activities;
	}
	public void setActivities(List<DailyActivityDTO> activities) {
		this.activities = activities;
	}

	
}
