package com.hcl.employee.service.feignI;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.hcl.employee.service.dto.DailyActivityDTO;

//@FeignClient(name = "http://ACTIVITY-SERVICE/dailyactivity-data/v1/activities")
@FeignClient(name = "http://ACTIVITY-SERVICE")
//@FeignClient(value = "activity-service", url = "http://localhost:8090/dailyactivity-data/v1/activities")
public interface DailyActivityClient {

	@GetMapping("/dailyactivity-data/v1/activities/{code}")
	public List<DailyActivityDTO> getActivityByCode(@PathVariable Integer code);
	
}
