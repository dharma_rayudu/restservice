package com.hcl.employee.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.employee.service.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	List<Employee> findByExperiance(Integer experiance);

	//List<Employee> findByEmployeeDesignationAndExperiance(String designation, String experiance);

	//List<Employee> findByIsActive(boolean isActive);

	Employee findByCode(Integer code);

	void deleteByCode(Integer code);

}
