package com.hcl.employee.service.exception;

import lombok.Data;

@Data
public class ErrorResponse {
	
	private String statusCode;
	private String statusMessage;

}
