package com.hcl.employee.service.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hcl.employee.service.dto.DailyActivityDTO;
import com.hcl.employee.service.dto.EmployeeDTO;
import com.hcl.employee.service.entity.Employee;
import com.hcl.employee.service.exception.ResourceNotFoundException;
import com.hcl.employee.service.feignI.DailyActivityClient;
import com.hcl.employee.service.repository.EmployeeRepository;
import com.hcl.employee.service.response.EmployeeActivites;
import com.hcl.employee.service.service.EmployeeService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	//@Autowired
	//private RestTemplate restTemplate;
	
	@Autowired
	private DailyActivityClient dailyActivityClient;

	@Override
	public ResponseEntity<EmployeeDTO> addEmployee(List<EmployeeDTO> employee) {
		log.info("Inside addEmployee method of EmployeeServiceImpl");

		List<Employee> empEntity = new ArrayList<Employee>();
		employee.forEach(emp -> {
			Employee empObj = new Employee();

			BeanUtils.copyProperties(emp, empObj);

			empEntity.add(empObj);

		});
		employeeRepository.saveAll(empEntity);

		return new ResponseEntity<EmployeeDTO>(HttpStatus.CREATED);

	}

	@Override
	public List<EmployeeDTO> getAllEmployees() {
		log.info("Inside getAllEmployees method of EmployeeServiceImpl");

		List<Employee> listEmployee = employeeRepository.findAll();
		return modelToDTOConverter(listEmployee);
	}

	@Override
	public EmployeeDTO getEmployeeByCode(Integer code) {
		log.info("Inside getEmployeeById method of EmployeeServiceImpl");
		//Employee emp = employeeRepository.findByCode(code);
		
		
		Employee empObject = employeeRepository.findByCode(code);
		if(empObject == null)
			throw new ResourceNotFoundException(code);
		
		EmployeeDTO empDTO = new EmployeeDTO();
		BeanUtils.copyProperties(empObject, empDTO);
		return empDTO;
	}

	@Override
	public EmployeeDTO updateEmployee(Integer code, Employee employee) {
		log.info("Inside updateEmployee method of EmployeeServiceImpl");
		Employee emp = employeeRepository.findByCode(code);//.orElseThrow(() -> new ResourceNotFoundException());
		EmployeeDTO empDTO = new EmployeeDTO();
		if(emp !=null) {
			emp.setJobTitle(employee.getJobTitle());
			emp.setExperiance(employee.getExperiance());
			emp.setPhoneNumber(employee.getPhoneNumber());
			emp.setProject_status(employee.getProject_status());

			Employee empObj = employeeRepository.save(emp);
			
			BeanUtils.copyProperties(empObj, empDTO);
		}else {
			throw new ResourceNotFoundException(code);
		}
		

		return empDTO;
	}

	@Override
	@Transactional
	public void deleteEmployee(Integer code) {
		log.info("Inside deleteEmployee method of EmployeeServiceImpl");
		Employee empObj = employeeRepository.findByCode(code);
		if(empObj !=null)
		// active or inactive

		//Employee emp = employeeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException());
		 employeeRepository.deleteByCode(code);
	//	emp.setActive(false);
		//employeeRepository.save(emp);

	}

	private List<EmployeeDTO> modelToDTOConverter(List<Employee> employee) {
		List<EmployeeDTO> listEmpDTO = new ArrayList<EmployeeDTO>();

		employee.forEach(list -> {

			EmployeeDTO empObj = new EmployeeDTO();
			BeanUtils.copyProperties(list, empObj);

			listEmpDTO.add(empObj);

		});

		return listEmpDTO;

	}

	@Override
	public List<EmployeeDTO> getEmployeesByExperiance(Integer experiance) {
		List<Employee> employeeList = employeeRepository.findByExperiance(experiance);

		return modelToDTOConverter(employeeList);
	}
	/*
	 * @Override public List<EmployeeDTO>
	 * getEmployeeByDesignationAndExperiance(String designation, String experiance)
	 * { List<Employee> employeeList =
	 * employeeRepository.findByEmployeeDesignationAndExperiance(designation,
	 * experiance); return modelToDTOConverter(employeeList); }
	 */

	@Override
	public String saveEmp(@Valid EmployeeDTO emp) {
		// TODO Auto-generated method stub
		Employee empObj = new Employee();
		
		BeanUtils.copyProperties(emp, empObj);
		
		employeeRepository.save(empObj);
		String out="Data Saved";
	return out;
		
		
	}

	/*
	 * @Override public List<EmployeeDTO> getActiveEmployees(boolean isActive) {
	 * log.info("Inside getActiveEmployees method of EmployeeServiceImpl");
	 * 
	 * List<Employee> empList = employeeRepository.findByIsActive(isActive); return
	 * modelToDTOConverter(empList); }
	 */

	@Override
	public EmployeeActivites getEmployeeActivites(Integer code) {
		EmployeeActivites empActivities = new EmployeeActivites();
		Employee emp = employeeRepository.findByCode(code);
		System.out.println("What is the code:" +emp.getCode());
		//DailyActivityDTO activity1 = restTemplate.getForObject("http://ACTIVITY-SERVICE/dailyactivity-data/v1/activities/"+emp.getCode(), DailyActivityDTO.class);
		List<DailyActivityDTO> activity  = dailyActivityClient.getActivityByCode(emp.getCode());
		System.out.println("What is the size:"+ activity.size());
		empActivities.setEmp(emp);
		//empActivities.setActivities(activity);
		empActivities.getActivities().addAll(activity);
		return empActivities;
	}

}
