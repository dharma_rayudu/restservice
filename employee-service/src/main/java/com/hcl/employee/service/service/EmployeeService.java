package com.hcl.employee.service.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.hcl.employee.service.dto.EmployeeDTO;
import com.hcl.employee.service.entity.Employee;
import com.hcl.employee.service.response.EmployeeActivites;

public interface EmployeeService {

	public ResponseEntity<EmployeeDTO> addEmployee(List<EmployeeDTO> employee);

	public List<EmployeeDTO> getAllEmployees();

	public EmployeeDTO getEmployeeByCode(Integer code);

	public EmployeeDTO updateEmployee(Integer id, Employee employee);

	public void deleteEmployee(Integer id);

	public List<EmployeeDTO> getEmployeesByExperiance(Integer experiance);

	//public List<EmployeeDTO> getEmployeeByDesignationAndExperiance(String designation, String experiance);

	public String saveEmp(@Valid EmployeeDTO emp);

	//public List<EmployeeDTO> getActiveEmployees(boolean isActive);
	
	public EmployeeActivites getEmployeeActivites(Integer code);

}
