package com.hcl.employee.service.exception;

import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomizedExceptionHandler extends ResponseEntityExceptionHandler{
	

	private static final String INVALIDDATA = null;

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<Object> handleException(ResourceNotFoundException eception, WebRequest webrequest){
		ExceptionResponse exceptionResponse = new ExceptionResponse();
		exceptionResponse.setMessage("No Employee Record Found");
		exceptionResponse.setStatus("NO Data");
		
		ResponseEntity<Object> entity = new ResponseEntity<Object>(exceptionResponse, HttpStatus.NOT_FOUND);
		return entity;
		
	}
	
	//@Override
   @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException argInvalidException,

                  HttpHeaders headers, HttpStatus httpStatus, WebRequest request) {

           ErrorResponse errorResponse = new ErrorResponse();

           errorResponse.setStatusCode("404");

           String allFieldErrors = argInvalidException.getBindingResult().getFieldErrors().stream()

                        .map(e -> e.getDefaultMessage()).collect(Collectors.joining(", "));

           errorResponse.setStatusMessage(allFieldErrors);

           return new ResponseEntity<Object>(errorResponse, HttpStatus.BAD_REQUEST);

    }
}
